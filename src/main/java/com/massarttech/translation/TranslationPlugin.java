package com.massarttech.translation;

import com.android.build.gradle.BaseExtension;
import groovy.util.Node;
import groovy.util.XmlParser;
import org.gradle.api.GradleException;
import org.gradle.api.Plugin;
import org.gradle.api.Project;

import java.io.*;
import java.util.Iterator;

public class TranslationPlugin implements Plugin<Project> {
    public void apply(Project project) {
        project.getTasks().create("makeTranslationData", TranslationOptions.class, (task) -> {
            findAndroidPlugin(project);
            BaseExtension extension = (BaseExtension) project.getExtensions().getByName("android");
            getStringsFile(extension);
        });
    }

    private void getStringsFile(BaseExtension baseExtension) {
        baseExtension.getSourceSets().all(androidSourceSet -> {
            File releaseDir = null;
            for (File file : androidSourceSet.getRes().getSrcDirs()) {
                if (file.getAbsolutePath().contains("main")) {
                    releaseDir = file;
                    break;
                }
            }
            if (releaseDir != null) {
                File stringsFile = new File(releaseDir, "values/strings.xml");
                if (stringsFile.exists()) {
                    File rawFile = new File(releaseDir, "raw");
                    rawFile.mkdir();
                    parseStringsFile(stringsFile, new File(rawFile, "translations.json"));
                }

            }
        });
    }

    private void parseStringsFile(File stringsFile, File translationFile) {
        try {
            createTranslationFile(translationFile);
            StringBuilder stringBuilder = new StringBuilder("{\n");
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(translationFile)));
            Node rootNode = new XmlParser().parse(stringsFile);
            Iterator iterator = rootNode.iterator();
            while (iterator.hasNext()) {
                Node node = (Node) iterator.next();
                if (isTranslatable(node)) {
                    writeNodeToBuilder(node, stringBuilder);
                }
            }
            stringBuilder.deleteCharAt(stringBuilder.lastIndexOf(","));
            stringBuilder.append("}");
            bufferedWriter.write(stringBuilder.toString());
            bufferedWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createTranslationFile(File translationFile) throws IOException {
        if (translationFile.exists()) {
            translationFile.delete();
        }
        translationFile.createNewFile();
    }

    private void writeNodeToBuilder(Node node, StringBuilder builder) {
        String name = (String) node.attribute("name");
        String value = node.text();
        if (value != null) {
            value = value
                    .replace("\\", "")
                    .replace("\"", "");

            System.out.println(name + " => " + value);
            builder.append("\"")
                    .append(value)
                    .append("\":")
                    .append("\"")
                    .append(name)
                    .append("\",\n");
        }
    }

    private boolean isTranslatable(Node node) {
        Object translatable = node.attribute("translatable");
        return translatable == null || translatable.toString().equals("true");
    }

    private Plugin findAndroidPlugin(Project project) {
        Plugin androidPlugin = null;
        String[] plugins = {"android", "com.android.application", "android-library", "com.android.library"
                , "com.android.test", "com.android.feature"};
        for (String plugin :
                plugins) {
            if (project.getPlugins().hasPlugin(plugin)) {
                androidPlugin = project.getPlugins().getPlugin(plugin);
                break;
            }
        }
        if (androidPlugin == null) {
            throw new GradleException("You must apply the Android " +
                    "plugin or the Android library plugin before using the groovy-android plugin");
        }
        return androidPlugin;
    }
}
